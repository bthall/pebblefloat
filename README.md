# PebbleFloat

A game that rewards patience. A pebble sits on the ground in front of you, but soon it will rise into the air ... if you will let it.